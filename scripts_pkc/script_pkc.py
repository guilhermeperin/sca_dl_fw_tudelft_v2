import sys
import os

sys.path.append('C:\\Users\\guilh\\PycharmProjects\\dl_sca_framework_v2')
# sys.path.append('/home/guilherme/sca_dl_fw_tudelft_v2')

import tensorflow as tf

print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))
print(tf.__version__)

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

from keras_models.neural_networks import NeuralNetwork
from commons.sca_deep_pkc import ScaDeepPKC


sca_deep = ScaDeepPKC()
sca_deep.target("ecc_ha_pointer")
sca_deep.set_database_name("database_pkc_all_multiple_gv_test.sqlite")
sca_deep.set_znorm()
sca_deep.set_mini_batch(100)
sca_deep.set_epochs(10)
sca_deep.keras_model(NeuralNetwork.cnn_ecc_ha_pointer, cnn=True)
sca_deep.run_recursive(50, data_augmentation=[200])


