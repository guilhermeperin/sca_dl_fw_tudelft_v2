import numpy as np
import pandas as pd
import time
import os
import h5py
import scipy.stats
from tensorflow.keras import backend
from tensorflow.keras.utils import to_categorical
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import TestCallback, GECallbackEpochEnd, InputGradients
from commons.sca_keras_models import ScaKerasModels
from commons.sca_functions import ScaFunctions
from database.database_inserts import DatabaseInserts


class ScaDeep:

    def __init__(self):
        self.target_trace_set = None
        self.sca_parameters = ScaParameters()
        self.target_params = None
        self.aes_lm = None
        self.crypto = AES()
        self.callbacks = []
        self.model = None
        self.random_model = None
        self.model_obj = None
        self.model_name = None
        self.database_name = None
        self.db_inserts = None
        self.callbacks = None

        self.metric_names = None
        self.keras_model_metrics = None

        self.ge_test = None
        self.sr_test = None
        self.sge_test = None
        self.gge_test = None
        self.metric_training = None
        self.metric_validation = None

        self.hyper_parameters = []
        self.random_hyper_parameters = None
        self.learning_rate = None
        self.optimizer = None

        self.mlp = None
        self.cnn = None

        self.ge_runs = 1
        self.sr_runs = 1
        self.target_key_bytes = None
        self.key_int = None
        self.first_key_byte = True
        self.key_rank_all_epochs_active = False
        self.visualization_active = False
        self.sge_gge_active = False

        self.z_score_mean = None
        self.z_score_std = None
        self.z_norm = False

        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    def target(self, target):
        self.target_trace_set = target
        self.sca_parameters = ScaParameters()
        self.target_params = self.sca_parameters.get_trace_set(self.target_trace_set)

    def set_database_name(self, database_name):
        self.database_name = database_name

    def sca_parameters(self):
        return self.sca_parameters

    def set_znorm(self):
        self.z_norm = True

    def set_mini_batch(self, mini_batch):
        self.target_params["mini-batch"] = mini_batch

    def set_epochs(self, epochs):
        self.target_params["epochs"] = epochs

    def set_train_traces(self, train_traces):
        self.target_params["n_train"] = train_traces

    def aes_leakage_model(self, leakage_model="HW", bit=0, byte=0):
        self.aes_lm = {
            "leakage_model": leakage_model,
            "bit": bit,
            "byte": byte,
        }

        if self.target_params is not None:
            if self.aes_lm["leakage_model"] == "HW":
                self.target_params["classes"] = 9
            elif self.aes_lm["leakage_model"] == "ID":
                self.target_params["classes"] = 256
            else:
                self.target_params["classes"] = 2
        else:
            print("Parameters (param) from target is not selected. Set target before the leakage model.")

        return self.aes_lm

    def add_callback(self, callback):
        self.callbacks.append(callback)
        return self.callbacks

    def clear_callbacks(self):
        self.callbacks = []

    def train_validation_sets(self):
        hf = h5py.File('../datasets/' + self.target_trace_set + '.h5', 'r')
        train_samples = np.array(hf.get('profiling_traces'))
        train_data = np.array(hf.get('profiling_data'))

        self.create_z_score_norm(train_samples)
        self.apply_z_score_norm(train_samples)

        training_dataset_reshaped = train_samples.reshape((train_samples.shape[0], train_samples.shape[1], 1))

        x_train = training_dataset_reshaped[0:self.target_params["n_train"]]
        x_val = training_dataset_reshaped[
                self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]
        y_train = train_data[0:self.target_params["n_train"]]
        y_validation = train_data[
                       self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]
        val_samples = train_samples[
                      self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]

        return x_train, x_val, train_samples[0:self.target_params["n_train"]], val_samples, y_train, y_validation

    def test_set(self, two_sets):
        hf = h5py.File('../datasets/' + self.target_trace_set + '.h5', 'r')
        test_samples = np.array(hf.get('attacking_traces'))
        test_data = np.array(hf.get('attacking_data'))

        self.apply_z_score_norm(test_samples)

        if two_sets:
            nt = int(self.target_params["n_test"] / 2)
            test_samples1 = test_samples[0:nt]
            test_samples2 = test_samples[nt:self.target_params["n_test"]]
            y_test1 = test_data[0:nt]
            y_test2 = test_data[nt:self.target_params["n_test"]]
            x_test1 = test_samples1.reshape((test_samples1.shape[0], test_samples1.shape[1], 1))
            x_test2 = test_samples2.reshape((test_samples2.shape[0], test_samples2.shape[1], 1))
            return x_test1, test_samples1, y_test1, x_test2, test_samples2, y_test2
        else:
            test_samples = test_samples[0:self.target_params["n_test"]]
            y_test = test_data[0:self.target_params["n_test"]]
            x_test = test_samples.reshape((test_samples.shape[0], test_samples.shape[1], 1))
            return x_test, test_samples, y_test, None, None, None

    def create_z_score_norm(self, dataset):
        self.z_score_mean = np.mean(dataset, axis=0)
        self.z_score_std = np.std(dataset, axis=0)

    def apply_z_score_norm(self, dataset):
        for index in range(len(dataset)):
            dataset[index] = (dataset[index] - self.z_score_mean) / self.z_score_std

    def keras_model(self, model, mlp=False, cnn=False):
        self.model_name = model
        self.model_obj = model
        self.model = model(self.target_params["classes"], self.target_params["number_of_samples"])
        self.mlp = mlp
        self.cnn = cnn

    def set_random_hyper_parameters(self, random_hyper_parameters):
        self.random_hyper_parameters = random_hyper_parameters

    def get_model(self):
        return self.model

    def get_test_guessing_entropy(self):
        return self.ge_test

    def get_test_success_rate(self):
        return self.sr_test

    def __set_metrics(self, key_ranking_all_epochs):
        self.metric_names = self.model.metrics_names
        if key_ranking_all_epochs:
            self.keras_model_metrics = len(self.metric_names) + 2
        else:
            self.keras_model_metrics = len(self.metric_names)

    def initialize_metric_training_validation(self):
        self.metric_training = np.zeros((self.keras_model_metrics, self.target_params["epochs"]))
        self.metric_validation = np.zeros((self.keras_model_metrics, self.target_params["epochs"]))

    def initialize_result_vectors(self, nt_test, nt_kr):
        self.ge_test = np.zeros(nt_kr)
        self.sr_test = np.zeros(nt_test)
        self.sge_test = np.zeros((self.target_params["number_of_key_hypothesis"], nt_kr))
        self.gge_test = np.zeros((self.target_params["number_of_key_hypothesis"], nt_kr))

    def initialize_configurations(self, target_key_bytes, ge_sr, key_ranking_all_epochs, sge_gge, visualization):

        if ge_sr is not None:
            self.ge_runs = ge_sr[0]

        self.key_int = ([int(x) for x in bytearray.fromhex(self.target_params["key"])])
        if target_key_bytes is None:
            self.target_key_bytes = list(range(0, len(self.key_int)))

        if key_ranking_all_epochs:
            self.key_rank_all_epochs_active = True

        if sge_gge:
            self.sge_gge_active = True

        if visualization:
            self.visualization_active = True

        self.first_key_byte = True

    def run(self, target_key_bytes=None, ge_sr=None, key_ranking_all_epochs=False, sge_gge=False, visualization=False):

        self.__set_metrics(key_ranking_all_epochs)
        self.hyper_parameters = []
        self.initialize_configurations(target_key_bytes, ge_sr, key_ranking_all_epochs, sge_gge, visualization)

        x_train, x_val, train_samples, val_samples, train_data, validation_data = self.train_validation_sets()
        x_test_1, test_samples_1, test_data_1, x_test_2, test_samples_2, test_data_2 = self.test_set(self.key_rank_all_epochs_active)

        x_t = train_samples if self.mlp else x_train
        x_v = val_samples if self.mlp else x_val
        x_t1 = test_samples_1 if self.mlp else x_test_1
        x_t2 = test_samples_2 if self.mlp else x_test_2

        report_ge_sr_interval = ge_sr[1]
        fraction_ge_sr_set = ge_sr[2]
        nt_test = len(test_samples_1)
        nt_kr = int(nt_test / (report_ge_sr_interval * fraction_ge_sr_set))

        self.initialize_metric_training_validation()
        self.initialize_result_vectors(nt_test, nt_kr)

        start = time.time()

        for key_byte_index in target_key_bytes:
            self.target_params["good_key"] = self.key_int[key_byte_index]
            self.aes_lm["byte"] = key_byte_index

            train_labels = self.crypto.aes_labelize(train_data, self.aes_lm, self.target_params)
            validation_labels = self.crypto.aes_labelize(validation_data, self.aes_lm, self.target_params)
            test_labels = self.crypto.aes_labelize(test_data_1, self.aes_lm, self.target_params)

            y_train = to_categorical(train_labels, num_classes=self.target_params["classes"])
            y_val = to_categorical(validation_labels, num_classes=self.target_params["classes"])
            y_test = to_categorical(test_labels, num_classes=self.target_params["classes"])

            self.clear_callbacks()

            callback_key_rank_validation = None
            callback_key_rank_test = None
            callback_input_gradients = None

            if self.key_rank_all_epochs_active:
                print("add callback")
                callback_key_rank_validation = GECallbackEpochEnd(x_t2, test_data_2, self.target_params,
                                                                  self.aes_lm, self.ge_runs, report_ge_sr_interval, fraction_ge_sr_set)
                callback_key_rank_test = GECallbackEpochEnd(x_t1, test_data_1, self.target_params,
                                                            self.aes_lm, self.ge_runs, report_ge_sr_interval, fraction_ge_sr_set)
                self.add_callback(callback_key_rank_validation)
                self.add_callback(callback_key_rank_test)

            if self.visualization_active:
                callback_input_gradients = InputGradients(x_t1[0:2000], y_train[0:2000], x_t[0:2000], self.target_params)
                self.add_callback(callback_input_gradients)

            callback_test = TestCallback(x_t1[0:int(len(x_t1) / ge_sr[2])], y_test[0:int(len(x_t1) / ge_sr[2])])
            callbacks = self.add_callback(callback_test)

            print("\nKey Byte {}\n".format(key_byte_index))

            os.environ["CUDA_VISIBLE_DEVICES"] = "0"

            history = self.model.fit(
                x=x_t,
                y=y_train,
                batch_size=self.target_params["mini-batch"],
                verbose=1,
                epochs=self.target_params["epochs"],
                shuffle=True,
                validation_data=(x_v, y_val),
                callbacks=callbacks)

            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

            model_name = self.model_obj.__name__
            self.learning_rate = backend.eval(self.model.optimizer.lr)
            self.optimizer = self.model.optimizer.__class__.__name__

            self.compute_ge_and_sr(x_t1, test_data_1, report_ge_sr_interval, fraction_ge_sr_set)
            self.set_hyper_parameters(nt_test, report_ge_sr_interval, fraction_ge_sr_set)
            self.get_metrics_results(history, callback_key_rank_validation, callback_key_rank_test)
            self.save_results_in_database(time.time() - start, model_name)
            self.save_metrics(callback_test.get_accuracy())
            self.save_results()

            if self.visualization_active:
                input_gradients_epoch = callback_input_gradients.grads_epoch()
                for epoch in range(self.target_params["epochs"]):
                    self.db_inserts.save_visualization(pd.Series(input_gradients_epoch[epoch]).to_json(), epoch, 0, "InputGradient")
                input_gradients_sum = callback_input_gradients.grads()
                self.db_inserts.save_visualization(pd.Series(input_gradients_sum / self.target_params["epochs"]).to_json(),
                                                   self.target_params["epochs"], 0, "InputGradient")

            backend.clear_session()

    def __save_metric_avg(self, metric, n_models, name):
        kr_avg = sum(metric[n] for n in range(n_models)) / n_models
        self.db_inserts.save_metric(kr_avg, self.aes_lm["byte"], name)

    def __save_metric(self, metric, name):
        self.db_inserts.save_metric(metric, self.aes_lm["byte"], name)

    def __save_kr(self, kr):
        self.db_inserts.save_key_rank_json(pd.Series(kr).to_json(), self.aes_lm["byte"])

    def __save_sr(self, sr):
        self.db_inserts.save_success_rate_json(pd.Series(sr).to_json(), self.aes_lm["byte"])

    def __save_ge(self, kr, name, kg):
        self.db_inserts.save_guessing_entropy_json(pd.Series(kr).to_json(), self.aes_lm["byte"], kg, name)

    def __save_final_ge(self, final_ge_test, final_ge_val):
        self.db_inserts.save_ensemble(pd.Series(final_ge_val).to_json(), self.aes_lm["byte"], "Validation")
        self.db_inserts.save_ensemble(pd.Series(final_ge_test).to_json(), self.aes_lm["byte"], "Test")

    def __set_hyper_parameters(self, key_rank):
        self.hyper_parameters.append({
            "key_rank": key_rank,
            "mini_batch": self.target_params["mini-batch"],
            "epochs": self.target_params["epochs"],
            "learning_rate": float(self.learning_rate),
            "optimizer": str(self.optimizer),
            "training_set": self.target_params["n_train"],
            "validation_set": self.target_params["n_validation"],
            "test_set": self.target_params["n_test"]
        })
        if self.random_hyper_parameters is not None:
            for key, value in self.random_hyper_parameters.items():
                self.hyper_parameters[0][key] = value
        self.hyper_parameters[0]["parameters"] = self.model.count_params()

    def save_results_in_database(self, elapsed_time, model_name):
        if self.first_key_byte:
            self.db_inserts = DatabaseInserts(self.database_name, self.target_params["name"],
                                              "script_single_all_keys.py", elapsed_time)

            leakage_model = [{
                "cipher": "AES",
                "model": self.aes_lm["leakage_model"],
                "operation": "S-Box"
            }]

            sca_keras_model = ScaKerasModels()
            keras_model_description = sca_keras_model.keras_model_as_string(model_name)

            self.db_inserts.save_neural_network(keras_model_description, model_name)
            self.db_inserts.save_hyper_parameters(self.hyper_parameters)
            self.db_inserts.save_leakage_model(leakage_model)

            self.first_key_byte = False
        else:
            self.db_inserts.update_elapsed_time_analysis(elapsed_time)

    def save_metrics(self, test_acc):
        m_index = 0
        for metric in self.metric_names:
            self.__save_metric(self.metric_training[m_index], metric)
            self.__save_metric(self.metric_validation[m_index], "val_" + metric)
            if metric == "acc":
                self.__save_metric(test_acc, "test_" + metric)
            m_index += 1
        if self.key_rank_all_epochs_active:
            self.__save_metric(self.metric_validation[m_index], "val_ge")
            m_index += 1
            self.__save_metric(self.metric_validation[m_index], "test_ge")

    def save_results(self):
        self.__save_kr(self.ge_test)
        self.__save_sr(self.sr_test)
        if self.sge_gge_active:
            for kg in range(self.target_params["number_of_key_hypothesis"]):
                self.__save_ge(self.sge_test[kg], "Static GE", kg)
                self.__save_ge(self.gge_test[kg], "Generalized GE", kg)

    def compute_ge_and_sr(self, x_t1, test_data_1, step, fraction):
        if self.sge_gge_active:
            SGE, self.sge_test, _ = ScaFunctions().sge_and_gge(100, self.model, self.target_params, self.aes_lm,
                                                               x_t1[0:int(len(x_t1) / fraction)], test_data_1[0:int(len(x_t1) / fraction)],
                                                               step, 1)
            GGE, self.gge_test, self.ge_test = ScaFunctions().sge_and_gge(self.ge_runs, self.model, self.target_params, self.aes_lm,
                                                                          x_t1, test_data_1, step, fraction)
            hwdd = ScaFunctions().calculate_HWDS(self.target_params["good_key"], None)
            iddd = ScaFunctions().calculate_IDDS(self.target_params["good_key"])

            SGE_sorted = np.argsort(np.argsort(SGE))
            GGE_sorted = np.argsort(np.argsort(GGE))
            hwdd_sorted = np.argsort(np.argsort(hwdd))
            iddd_sorted = np.argsort(np.argsort(iddd))

            print("corr(SGE,HWDD) = {}".format(scipy.stats.pearsonr(SGE_sorted, hwdd_sorted)[0]))
            print("corr(GGE,HWDD) = {}".format(scipy.stats.pearsonr(GGE_sorted, hwdd_sorted)[0]))

            print("corr(SGE,IDDD) = {}".format(scipy.stats.pearsonr(SGE_sorted, iddd_sorted)[0]))
            print("corr(GGE,IDDD) = {}".format(scipy.stats.pearsonr(GGE_sorted, iddd_sorted)[0]))
        else:
            self.ge_test, self.sr_test = ScaFunctions().ge_and_sr(self.ge_runs, self.model, self.target_params,
                                                                  self.aes_lm, x_t1, test_data_1, step, fraction)

    def set_hyper_parameters(self, nt_test, step, fraction):
        self.__set_hyper_parameters(self.ge_test[int(nt_test / (step * fraction)) - 1])

    def get_metrics_results(self, history, callback_key_rank_validation, callback_key_rank_test):
        m_index = 0
        for metric in self.metric_names:
            self.metric_training[m_index] = history.history[metric]
            self.metric_validation[m_index] = history.history['val_' + metric]
            m_index += 1

        if self.key_rank_all_epochs_active:
            self.metric_validation[m_index] = callback_key_rank_validation.get_final_ge_epochs()
            m_index += 1
            self.metric_validation[m_index] = callback_key_rank_test.get_final_ge_epochs()
