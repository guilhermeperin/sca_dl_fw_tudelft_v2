from tensorflow.keras.callbacks import Callback
import tensorflow.keras.backend as backend
from tensorflow.keras.layers import Input
import numpy as np
from crypto.aes import AES
from commons.sca_functions import ScaFunctions
from termcolor import colored
from scipy import stats
import matplotlib.pyplot as plt


class TestCallback(Callback):
    def __init__(self, x_data, y_data, save_model=False):
        self.current_epoch = 0
        self.x = x_data
        self.y = y_data
        self.accuracy = []
        self.recall = []
        self.loss = []
        self.save_model = save_model

    def on_epoch_end(self, epoch, logs={}):
        # self.accuracy.append(logs.get('val_acc'))
        # self.recall.append(logs.get('val_recall'))
        # self.loss.append(logs.get('val_loss'))

        loss, acc, recall = self.model.evaluate(self.x, self.y, verbose=0)

        self.accuracy.append(acc)
        self.recall.append(recall)
        self.loss.append(loss)

    def get_accuracy(self):
        return self.accuracy

    def get_recall(self):
        return self.recall

    def get_loss(self):
        return self.loss


class GECallbackEpochEnd(Callback):
    def __init__(self, x_val, x_labels, param, aes_leakage_model, ge_runs, step, fraction):
        self.current_epoch = 0
        self.x_val = x_val
        self.x_labels = x_labels
        self.nt = int(len(x_val) / (step * fraction))
        self.ge_epochs = np.zeros((param["epochs"], self.nt))
        self.final_ge_epochs = np.zeros((param["epochs"]))
        self.es_best_val_kr_epoch = 0
        self.param = param
        self.aes_leakage_model = aes_leakage_model
        self.ge_runs = ge_runs
        self.step = step
        self.fraction = fraction

        crypto = AES()
        self.labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], len(x_val)))
        for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
            key_h = bytearray.fromhex(param["key"])
            key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
            crypto.set_key(key_h)
            self.labels_key_hypothesis[key_byte_hypothesis][:] = crypto.aes_labelize(x_labels, aes_leakage_model, param,
                                                                                     key_hypothesis=True)

    def on_epoch_end(self, epoch, logs={}):
        self.ge_epochs[epoch], _, = ScaFunctions().ge_and_sr(self.ge_runs,
                                                             self.model,
                                                             self.param,
                                                             self.aes_leakage_model,
                                                             self.x_val,
                                                             self.x_labels,
                                                             self.step, self.fraction,
                                                             labels_kg=self.labels_key_hypothesis)

        self.final_ge_epochs[epoch] = self.ge_epochs[epoch][self.nt - 1]

    def get_final_ge_epochs(self):
        return self.final_ge_epochs


class CalculateAccuracyCurve25519(Callback):
    def __init__(self, x_data, y_true, y_ha, number_of_epochs):
        self.x_data = x_data
        self.labels_true = y_true
        self.labels_ha = y_ha
        self.max_accuracy = 0
        self.min_accuracy = 1
        self.avg_accuracy = 0
        self.max_accuracy_per_epoch = np.zeros(number_of_epochs)
        self.min_accuracy_per_epoch = np.zeros(number_of_epochs)
        self.accuracy_per_epoch = np.zeros(number_of_epochs)
        self.nbits_per_trace = 255
        self.number_of_validation_traces = int(len(self.labels_true) / self.nbits_per_trace)
        self.correct_bits_dl = np.zeros((self.number_of_validation_traces, number_of_epochs))
        self.correct_bits_ha = np.zeros((self.number_of_validation_traces, number_of_epochs))
        self.max_ttest = []
        self.x_samples = self.x_data.reshape(self.x_data.shape[0], self.x_data.shape[1])
        self.labels_1 = []
        self.epochs = number_of_epochs

    def on_epoch_end(self, epoch, logs=None):

        output_probabilities = self.model.predict(self.x_data)

        for trace_index in range(self.number_of_validation_traces):
            trace_probabilities = output_probabilities[
                                  trace_index * self.nbits_per_trace:(trace_index + 1) * self.nbits_per_trace]
            trace_labels_true = self.labels_true[
                                trace_index * self.nbits_per_trace:(trace_index + 1) * self.nbits_per_trace]
            trace_labels_ha = self.labels_ha[
                              trace_index * self.nbits_per_trace:(trace_index + 1) * self.nbits_per_trace]

            labels_1 = 0

            for bit_index in range(len(trace_probabilities)):
                if trace_probabilities[bit_index][1] > 0.5 and trace_labels_true[bit_index] == 1:
                    self.correct_bits_dl[trace_index][epoch] += 1
                    labels_1 += 1
                if trace_probabilities[bit_index][0] > 0.5 and trace_labels_true[bit_index] == 0:
                    self.correct_bits_dl[trace_index][epoch] += 1
                if trace_labels_true[bit_index] == trace_labels_ha[bit_index]:
                    self.correct_bits_ha[trace_index][epoch] += 1

            if self.correct_bits_dl[trace_index][epoch] / self.nbits_per_trace > 0.6:
                print(colored("Correct rate trace {} :{} / ha: {} (bits 1: {})".format(trace_index,
                                                                                       self.correct_bits_dl[trace_index][
                                                                                           epoch] / self.nbits_per_trace,
                                                                                       self.correct_bits_ha[trace_index][
                                                                                           epoch] / self.nbits_per_trace,
                                                                                       labels_1), 'green'))
            else:
                print("Correct rate trace {} :{} / ha: {} (bits 1: {})".format(trace_index,
                                                                               self.correct_bits_dl[trace_index][
                                                                                   epoch] / self.nbits_per_trace,
                                                                               self.correct_bits_ha[trace_index][
                                                                                   epoch] / self.nbits_per_trace,
                                                                               labels_1))

            self.correct_bits_dl[trace_index][epoch] /= self.nbits_per_trace
            self.correct_bits_ha[trace_index][epoch] /= self.nbits_per_trace

            if self.correct_bits_dl[trace_index][epoch] > self.max_accuracy:
                self.max_accuracy = self.correct_bits_dl[trace_index][epoch]

            if self.correct_bits_dl[trace_index][epoch] < self.min_accuracy:
                self.min_accuracy = self.correct_bits_dl[trace_index][epoch]

            self.max_accuracy_per_epoch[epoch] = self.max_accuracy
            self.min_accuracy_per_epoch[epoch] = self.min_accuracy

            if epoch == self.epochs - 1:
                self.avg_accuracy += self.correct_bits_dl[trace_index][epoch]

        self.avg_accuracy /= self.number_of_validation_traces

        print("\nMax Accuracy: " + str(self.max_accuracy))

    def get_correct_dl(self):
        return self.correct_bits_dl

    def get_correct_ha(self):
        return self.correct_bits_ha

    def get_max_accuracy(self):
        return self.max_accuracy

    def get_min_accuracy(self):
        return self.min_accuracy

    def get_avg_accuracy(self):
        return self.avg_accuracy

    def get_max_accuracy_per_epoch(self):
        return self.max_accuracy_per_epoch

    def get_min_accuracy_per_epoch(self):
        return self.min_accuracy_per_epoch

    def get_accuracy_per_epoch(self):
        return self.accuracy_per_epoch


class InputGradients(Callback):
    def __init__(self, x_data, y_data, y_labels, param):
        self.current_epoch = 0
        self.x = x_data
        self.y = y_data
        self.y_labels = y_labels
        self.param = param
        self.gradients = np.zeros((self.param["epochs"], self.param["number_of_samples"]))
        self.gradients_sum = np.zeros(self.param["number_of_samples"])

    def on_epoch_end(self, epoch, logs=None):
        print("Computing gradients...")

        # an input layer to feed labels
        y_true = Input(shape=(self.param["classes"],))
        # compute loss based on model's output and true labels
        loss = backend.mean(backend.categorical_crossentropy(y_true, self.model.output))
        # compute gradient of loss with respect to inputs
        grad_ce = backend.gradients(loss, self.model.inputs)
        # create a function to be able to run this computation graph
        func = backend.function(self.model.inputs + [y_true], grad_ce)

        output = func([self.x, self.y])

        grad = output[0]
        grad = grad.reshape(len(self.x), self.param["number_of_samples"])

        print("Processing gradients...")
        input_gradients = np.zeros(self.param["number_of_samples"])
        for i in range(len(self.x)):
            input_gradients += grad[i]

        self.gradients[epoch] = input_gradients
        if np.max(self.gradients[epoch]) != 0:
            self.gradients_sum += np.abs(self.gradients[epoch] / np.max(self.gradients[epoch]))
        else:
            self.gradients_sum += np.abs(self.gradients[epoch])

        backend.clear_session()

    def grads(self):
        return np.abs(self.gradients_sum)

    def grads_epoch(self):
        for e in range(self.param["epochs"]):
            if np.max(self.gradients[e]) != 0:
                self.gradients[e] = np.abs(self.gradients[e] / np.max(self.gradients[e]))
            else:
                self.gradients[e] = np.abs(self.gradients[e])
        return self.gradients
