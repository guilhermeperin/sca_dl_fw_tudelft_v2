from commons.sca_database import ScaDatabase
from commons.sca_parameters import ScaParameters
from database.tables import KeyRankJSON, SuccessRateJSON, Metric, AccuracyJSON, AccuracyIterationJSON, Visualization
from plots.PlotlyPlots import PlotlyPlots


class ScaViewsPKC:

    def __init__(self, analysis_id, db):
        self.db = db
        self.analysis_id = analysis_id

    def accuracy_pkc_plots(self):

        plotly_plots = PlotlyPlots()

        all_accuracy_set1_plots = []
        all_accuracy_set2_plots = []
        accuracy_pkc_plots = []

        all_accuracy_values = self.db.select_values_from_accuracy_json(AccuracyJSON, self.analysis_id)
        for all_accuracy_value in all_accuracy_values:
            if "acc_dl_set1_" in all_accuracy_value['name']:
                all_accuracy_set1_plots.append(
                    plotly_plots.create_line_plot(y=all_accuracy_value['values'], line_name=all_accuracy_value['name']))
            if "acc_dl_set2_" in all_accuracy_value['name']:
                all_accuracy_set2_plots.append(
                    plotly_plots.create_line_plot(y=all_accuracy_value['values'], line_name=all_accuracy_value['name']))

        accuracy_pkc_plots.append({
            "title": "Accuracy Deep Learning Set1",
            "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "Accuracy DL Set1"),
            "plots": all_accuracy_set1_plots
        })

        if len(all_accuracy_set2_plots) > 0:
            accuracy_pkc_plots.append({
                "title": "Accuracy Deep Learning Set2",
                "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "Accuracy DL Set2"),
                "plots": all_accuracy_set2_plots
            })

        return accuracy_pkc_plots

    def accuracy_iteration_pkc_plots(self):

        plotly_plots = PlotlyPlots()

        all_accuracy_set_plots = []
        accuracy_pkc_plots = []

        all_accuracy_values = self.db.select_values_from_accuracy_json(AccuracyIterationJSON, self.analysis_id)
        for all_accuracy_value in all_accuracy_values:
            all_accuracy_set_plots.append(
                plotly_plots.create_line_plot(y=all_accuracy_value['values'], line_name=all_accuracy_value['name']))

        accuracy_pkc_plots.append({
            "title": "Accuracy vs Iteration",
            "layout_plotly": plotly_plots.get_plotly_layout("Iteration", "Accuracy"),
            "plots": all_accuracy_set_plots
        })

        return accuracy_pkc_plots

    def visualization_plots(self):

        plotly_plots = PlotlyPlots()

        all_visualization_plots = []
        visualization_plots = []

        visualization_all_key_bytes = self.db.select_values_from_analysis_json(Visualization, self.analysis_id)
        for visualization_key_byte in visualization_all_key_bytes:
            visualization_plots_metrics = []
            # epoch = 0
            # for visualization in visualization_key_byte:
            #     visualization_plots_metrics.append(
            #         plotly_plots.create_line_plot(y=visualization['values'],
            #                                       line_name="kb" + str(visualization['key_byte']) + " IG - epoch " +
            #                                                 str(epoch)))
            #     epoch += 1
            visualization = visualization_key_byte[len(visualization_key_byte) - 1]
            visualization_plots_metrics.append(
                plotly_plots.create_line_plot(y=visualization['values'], line_name=str(visualization['key_byte']) + " IG - Sum"))
            visualization_plots.append(visualization_plots_metrics)

        all_visualization_plots.append({
            "title": "Visualization",
            "layout_plotly": plotly_plots.get_plotly_layout("Traces", "Gradient"),
            "plots": visualization_plots
        })

        return all_visualization_plots

    def visualization_plots_heatmap(self):

        plotly_plots = PlotlyPlots()

        all_visualization_plots = []
        visualization_plots = []

        visualization_all_key_bytes = self.db.select_values_from_analysis_json(Visualization, self.analysis_id)
        for visualization_key_byte in visualization_all_key_bytes:
            visualization_plots_metrics = []
            z = []
            x = list(range(len(visualization_key_byte[0]['values'])))
            y = []
            epoch = 0
            for visualization in visualization_key_byte:
                z.append(visualization['values'])
                y.append(epoch)
                epoch += 1
            visualization_plots_metrics.append(plotly_plots.create_heatmap(x=x, y=y, z=z))
            visualization_plots.append(visualization_plots_metrics)

        all_visualization_plots.append({
            "title": "Visualization",
            "layout_plotly": plotly_plots.get_plotly_layout("Samples", "Epoch"),
            "plots": visualization_plots
        })

        return all_visualization_plots