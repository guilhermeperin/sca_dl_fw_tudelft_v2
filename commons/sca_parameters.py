class ScaParameters:

    def __init__(self):
        self.trace_set_list = []

    def get_trace_set(self, trace_set_name):
        trace_list = self.get_trace_set_list()
        return trace_list[trace_set_name]

    def get_basic_metrics(self):
        return ["accuracy", "val_accuracy", "test_accuracy", "recall", "val_recall", "loss", "val_loss"]

    def get_trace_set_list(self):

        parameters_ascad_fixed_key = {
            "name": "ascad_fixed_key",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 32,
            "input_offset": 0,
            "data_length": 32,
            "first_sample": 0,
            "number_of_samples": 700,
            "n_train": 50000,
            "n_validation": 1000,
            "n_test": 5000,
            "classes": 9,
            "good_key": 224,
            "number_of_key_hypothesis": 256,
            "epochs": 50,
            "mini-batch": 50,
            "random_key": False
        }

        parameters_ascad_random_key = {
            "name": "ascad_random_key",
            "key": "00112233445566778899AABBCCDDEEFF",
            "key_offset": 16,
            "input_offset": 0,
            "data_length": 50,
            "first_sample": 0,
            "number_of_samples": 1400,
            "n_train": 2000,
            "n_validation": 1000,
            "n_test": 20000,
            "classes": 9,
            "good_key": 34,
            "number_of_key_hypothesis": 256,
            "epochs": 5,
            "mini-batch": 400,
            "random_key": True
        }

        parameters_ches_ctf = {
            "name": "ches_ctf",
            "key": "2EEE5E799D72591C4F4C10D8287F397A",
            "key_offset": 32,
            "input_offset": 0,
            "data_length": 48,
            "first_sample": 0,
            "number_of_samples": 2200,
            "n_train": 45000,
            "n_validation": 0,
            "n_test": 5000,  # 5000 available
            "classes": 9,
            "good_key": 46,
            "number_of_key_hypothesis": 256,
            "epochs": 20,
            "mini-batch": 400,
            "random_key": True
        }

        parameters_dpa_v4 = {
            "name": "dpa_v4",
            "key": "6cecc67f287d083deb8766f0738b36cf",
            "key_offset": 32,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 32,
            "first_sample": 0,
            "number_of_samples": 2000,
            "n_train": 36000,
            "n_validation": 0,
            "n_test": 4000,
            "classes": 9,
            "good_key": 108,
            "number_of_key_hypothesis": 256,
            "epochs": 50,
            "mini-batch": 400,
            "random_key": False
        }

        parameters_aes_hd_1 = {
            "name": "aes_hd_1",
            "key": "2b7e151628aed2a6abf7158809cf4f3c",
            "key_offset": 16,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 32,
            "first_sample": 0,
            "number_of_samples": 1250,
            "n_train": 50000,
            "n_validation": 1000,
            "n_test": 5000,
            "classes": 9,
            "good_key": 43,
            "number_of_key_hypothesis": 256,
            "epochs": 50,
            "mini-batch": 400,
            "random_key": False
        }

        parameters_aes_hd_2 = {
            "name": "aes_hd_2",
            "key": "2b7e151628aed2a6abf7158809cf4f3c",
            "key_offset": 16,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 32,
            "first_sample": 0,
            "number_of_samples": 1250,
            "n_train": 44000,
            "n_validation": 1000,
            "n_test": 5000,
            "classes": 9,
            "good_key": 43,
            "number_of_key_hypothesis": 256,
            "epochs": 50,
            "mini-batch": 400,
            "random_key": False
        }

        parameters_ecc_ha_winres = {
            "name": "ecc_ha_winres",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 800,
            "n_train": 31875,
            "n_validation": 31875,
            "n_test": 12750,
            "classes": 2,
            "epochs": 10,
            "mini-batch": 400
        }

        parameters_ecc_ha = {
            "name": "ecc_ha",
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 8000,
            "n_train": 31875,
            "n_validation": 31875,
            "n_test": 12750,
            "classes": 2,
            "epochs": 10,
            "mini-batch": 400
        }

        parameters_ecc_ha_winres_no_opt = {
            "name": "ecc_ha_winres_no_opt",
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 800,
            "n_train": 10200,
            "n_validation": 10200,
            "n_test": 5100,
            "classes": 2,
            "epochs": 25,
            "mini-batch": 64
        }

        parameters_ecc_ha_no_opt = {
            "name": "ecc_ha_no_opt",
            "data_length": 2,
            "first_sample": 1500,
            "number_of_samples": 2000,
            "n_train": 31875,
            "n_validation": 31875,
            "n_test": 12750,
            "classes": 2,
            "epochs": 25,
            "mini-batch": 64
        }

        parameters_ecc_ha_pointer = {
            "name": "ecc_ha_pointer",
            "data_length": 2,
            "first_sample": 550,
            "number_of_samples": 250,
            "n_train": 31875,
            "n_validation": 31875,
            "n_test": 12750,
            "classes": 2,
            "epochs": 25,
            "mini-batch": 64
        }

        parameters_ecc_ha_winres_pointer = {
            "name": "ecc_ha_winres_pointer",
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 1000,
            "n_train": 10200,
            "n_validation": 10200,
            "n_test": 5100,
            "classes": 2,
            "epochs": 25,
            "mini-batch": 64
        }

        self.trace_set_list = {
            "ascad_fixed_key": parameters_ascad_fixed_key,
            "ascad_random_key": parameters_ascad_random_key,
            "ches_ctf": parameters_ches_ctf,
            "dpa_v4": parameters_dpa_v4,
            "aes_hd_1": parameters_aes_hd_1,
            "aes_hd_2": parameters_aes_hd_2,
            # PKC
            "ecc_ha": parameters_ecc_ha,
            "ecc_ha_winres": parameters_ecc_ha_winres,
            "ecc_ha_winres_no_opt": parameters_ecc_ha_winres_no_opt,
            "ecc_ha_no_opt": parameters_ecc_ha_no_opt,
            "ecc_ha_pointer": parameters_ecc_ha_pointer,
            "ecc_ha_winres_pointer": parameters_ecc_ha_winres_pointer
        }

        return self.trace_set_list
