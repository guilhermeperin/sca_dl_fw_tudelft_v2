import numpy as np
import random
from crypto.aes import AES
from sklearn.utils import shuffle
import h5py


class ScaFunctions:

    def ge_and_sr(self, runs, model, param, aes_leakage_model, x_test, y_test, step, fraction, labels_kg=None):

        nt = len(x_test)
        nt_kr = int(nt / fraction)
        nt_interval = int(nt / (step * fraction))
        key_ranking_sum = np.zeros(nt_interval)
        success_rate_sum = np.zeros(nt_interval)

        # ---------------------------------------------------------------------------------------------------------#
        # compute labels for key hypothesis
        # ---------------------------------------------------------------------------------------------------------#
        if labels_kg is None:
            crypto = AES()
            labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], nt))
            for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
                key_h = bytearray.fromhex(param["key"])
                key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
                crypto.set_key(key_h)
                labels_key_hypothesis[key_byte_hypothesis][:] = crypto.aes_labelize(y_test,
                                                                                    aes_leakage_model,
                                                                                    param,
                                                                                    key_hypothesis=True)
        else:
            labels_key_hypothesis = labels_kg

        # ---------------------------------------------------------------------------------------------------------#
        # predict output probabilities for shuffled test or validation set
        # ---------------------------------------------------------------------------------------------------------#
        output_probabilities = model.predict(x_test)

        probabilities_kg_all_traces = np.zeros((nt, param["number_of_key_hypothesis"]))
        for index in range(nt):
            probabilities_kg_all_traces[index] = output_probabilities[index][
                np.asarray([int(leakage[index]) for leakage in labels_key_hypothesis[:]])
            ]

        for run in range(runs):

            probabilities_kg_all_traces_shuffled = shuffle(probabilities_kg_all_traces,
                                                           random_state=random.randint(0, 100000))

            key_probabilities = np.zeros(param["number_of_key_hypothesis"])

            kr_count = 0
            for index in range(nt_kr):

                key_probabilities += np.log(probabilities_kg_all_traces_shuffled[index] + 1e-36)
                key_probabilities_sorted = np.argsort(key_probabilities)[::-1]

                # if (index + 1) % step == 0 and index > 0:
                if (index + 1) % step == 0:
                    key_ranking_good_key = list(key_probabilities_sorted).index(param["good_key"]) + 1
                    key_ranking_sum[kr_count] += key_ranking_good_key

                    if key_ranking_good_key == 1:
                        success_rate_sum[kr_count] += 1

                    kr_count += 1

            print(
                "Computing Guessing Entropy - KR: {} | final GE for correct key ({}): {})".format(run, param["good_key"],
                                                                                                  key_ranking_sum[nt_interval - 1] / (
                                                                                                          run + 1)))

        guessing_entropy = key_ranking_sum / runs
        success_rate = success_rate_sum / runs

        hf = h5py.File('output_prob.h5', 'w')
        hf.create_dataset('output_probabilities', data=output_probabilities)
        hf.close()

        return guessing_entropy, success_rate

    def sge_and_gge(self, runs, model, param, aes_leakage_model, x_test, y_test, step, fraction, labels_kg=None):

        nt = len(x_test)
        nt_kr = int(nt / fraction)
        nt_interval = int(nt / (step * fraction))

        key_ranking_sum = np.zeros((param["number_of_key_hypothesis"], nt_interval))
        key_rankings = np.zeros((param["number_of_key_hypothesis"], runs, nt_interval))

        # ---------------------------------------------------------------------------------------------------------#
        # compute labels for key hypothesis
        # ---------------------------------------------------------------------------------------------------------#
        if labels_kg is None:
            crypto = AES()
            labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], nt))
            for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
                key_h = bytearray.fromhex(param["key"])
                key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
                crypto.set_key(key_h)
                labels_key_hypothesis[key_byte_hypothesis][:] = crypto.aes_labelize(y_test,
                                                                                    aes_leakage_model,
                                                                                    param,
                                                                                    key_hypothesis=True)
        else:
            labels_key_hypothesis = labels_kg

        # ---------------------------------------------------------------------------------------------------------#
        # predict output probabilities for shuffled test or validation set
        # ---------------------------------------------------------------------------------------------------------#
        output_probabilities = model.predict(x_test)

        probabilities_kg_all_traces = np.zeros((nt, param["number_of_key_hypothesis"]))
        for index in range(nt):
            probabilities_kg_all_traces[index] = output_probabilities[index][
                np.asarray([int(leakage[index]) for leakage in labels_key_hypothesis[:]])
            ]

        for run in range(runs):

            probabilities_kg_all_traces_shuffled = shuffle(probabilities_kg_all_traces,
                                                           random_state=random.randint(0, 100000))

            key_probabilities = np.zeros(param["number_of_key_hypothesis"])

            kr_count = 0
            for index in range(nt_kr):

                key_probabilities += np.log(probabilities_kg_all_traces_shuffled[index] + 1e-36)
                key_probabilities_sorted = np.argsort(key_probabilities)[::-1]

                if (index + 1) % step == 0 and index > 0:
                    for kg in range(param["number_of_key_hypothesis"]):
                        key_ranking_sum[kg][kr_count] += list(key_probabilities_sorted).index(kg) + 1
                        key_rankings[kg][run][kr_count] = list(key_probabilities_sorted).index(kg) + 1
                    kr_count += 1

            fkr = key_ranking_sum[param["good_key"]][nt_interval - 1] / (run + 1)
            print(
                "Key rank: {} | final guessing entropy for correct key ({}): ""{})".format(run, param["good_key"], fkr))

        final_guessing_entropy = np.zeros(param["number_of_key_hypothesis"])
        guessing_entropy = np.zeros((param["number_of_key_hypothesis"], nt_interval))
        for kg in range(param["number_of_key_hypothesis"]):
            final_guessing_entropy[kg] = key_ranking_sum[kg][nt_interval - 1] / runs
            guessing_entropy[kg] = key_ranking_sum[kg] / runs

        return final_guessing_entropy, guessing_entropy, guessing_entropy[param["good_key"]]

    def calculate_HWDS(self, k_c, container):

        p = list(range(256))
        for i in range(len(p)):
            p[i] = p[i] % 256

        hw = [bin(x).count("1") for x in range(256)]
        k_all = range(256)

        if container is None:
            container = np.zeros((len(k_all), len(p)), int)

            for i in range(256):
                for j in range(len(k_all)):
                    container[j][i] = hw[AES().aes_sbox_table()[p[i] ^ k_all[j]]]

        hdws = np.zeros((256,))
        for k in range(256):
            hdws[k] = np.sum(abs((container[k_c] - container[k]) * (container[k_c] - container[k])))

        return hdws

    def calculate_IDDS(self, k_c):

        p = list(range(256))
        for i in range(len(p)):
            p[i] = p[i] % 256

        k_all = range(256)

        container = np.zeros((len(k_all), len(p)), int)

        for i in range(len(p)):
            for j in range(len(k_all)):
                container[j][i] = AES().aes_sbox_table()[p[i] ^ k_all[j]]

        hdws = np.zeros((256,))
        for k in range(256):
            hdws[k] = np.sum(abs((container[k_c] - container[k])*(container[k_c] - container[k])))

        return hdws

    def calculate_bitDS(self, k_c):

        p = list(range(256))
        for i in range(len(p)):
            p[i] = p[i] % 256

        k_all = range(256)

        container = np.zeros((len(k_all), len(p)), int)

        for i in range(len(p)):
            for j in range(len(k_all)):
                container[j][i] = AES().aes_sbox_table()[p[i] ^ k_all[j]]
                container[j][i] = int(bin(container[j][i] >> 0)[len(bin(container[j][i] >> 0)) - 1])

        hdws = np.zeros((256,))
        for k in range(256):
            hdws[k] = np.sum(abs((container[k_c] - container[k])*(container[k_c] - container[k])))

        return hdws
