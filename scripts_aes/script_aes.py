import sys

# sys.path.append('/home/nfs/gperin/sca_dl_fw_tudelft_v2')
sys.path.append('C:\\Users\\guilh\\PycharmProjects\\dl_sca_framework_v2')

from keras_models.neural_networks import NeuralNetwork
from commons.sca_deep_aes import ScaDeep

import tensorflow as tf

print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

sca_deep = ScaDeep()
sca_deep.target("ascad_random_key")
sca_deep.set_database_name("database.sqlite")
sca_deep.aes_leakage_model(leakage_model="HW")
sca_deep.set_train_traces(44000)
sca_deep.set_mini_batch(200)
sca_deep.set_epochs(50)
neural_network = NeuralNetwork()
sca_deep.keras_model(neural_network.mlp_small_sgd_momentum_elu, mlp=True)
sca_deep.run(
    target_key_bytes=[2],
    ge_sr=[100, 250, 5],
    key_ranking_all_epochs=True,
    # sge_gge=True
)
