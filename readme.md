Download .h5 datasets from: https://www.dropbox.com/s/h9xmvwvvw7v36md/datasets.zip?dl=0

into 'datasets' folders. Next, run the python script:
 
```
python unzip_datasets.py
```
to unzip the dataset files.
Datasets contained in the datasets.zip file are:
- ascad_fixed_key.h5
- ascad_random_key.h5
- ecc_ha_pointer.h5
- ecc_ha.h5

To start the webapp, type in the terminal:
```
flask run
```

It will open a localhost link to visualize the database results.