from tensorflow.keras import backend as backend
from tensorflow.keras.optimizers import Adam, RMSprop, SGD, Adagrad, Adadelta
from tensorflow.keras.layers import Flatten, Dense, Input, Dropout, GaussianNoise
from tensorflow.keras.layers import Conv1D, Conv2D, AveragePooling2D, MaxPooling1D, MaxPooling2D, BatchNormalization, AveragePooling1D
from keras_adabound import AdaBound
from tensorflow.keras.models import Sequential
import random


class NeuralNetwork:

    def __init__(self):
        self.activation_function = "relu"
        self.filters = 1
        self.kernel_size = 1
        self.stride = 1
        self.learning_rate = 1e-4
        self.neurons = 100
        self.conv_layers = 1
        self.dense_layers = 1
        self.optimizer = "RMSprop"

    def get_random_hyper_parameters(self):
        return {
            "activation_function": self.activation_function,
            "filters": self.filters,
            "stride": self.stride,
            "kernel_size": self.kernel_size,
            "learning_rate": self.learning_rate,
            "neurons": self.neurons,
            "conv_layers": self.conv_layers,
            "dense_layers": self.dense_layers,
            "optimizer": self.optimizer.__class__.__name__
        }

    def recall(self, classes):
        def recall(y_true, y_pred):
            class_acc = 0
            for class_id in range(classes):
                class_id_true = backend.argmax(y_true, axis=-1)
                class_id_preds = backend.argmax(y_pred, axis=-1)
                # Replace class_id_preds with class_id_true for recall here
                accuracy_mask = backend.cast(backend.equal(class_id_true, class_id), 'int32')
                class_acc_tensor = backend.cast(backend.equal(class_id_true, class_id_preds), 'int32') * accuracy_mask
                class_acc += backend.sum(class_acc_tensor) / backend.maximum(backend.sum(accuracy_mask), 1)
            return class_acc / classes

        return recall

    def mlp_small_sgd_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_small_sgd_nesterov_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_small_sgd_momentum_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_small_sgd_momentum_nesterov_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_large_sgd_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_large_sgd_nesterov_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_large_sgd_momentum_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_large_sgd_momentum_nesterov_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_small_sgd_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='elu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_small_sgd_nesterov_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='elu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_small_sgd_momentum_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='elu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_small_sgd_momentum_nesterov_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='elu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_large_sgd_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_large_sgd_nesterov_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_large_sgd_momentum_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_large_sgd_momentum_nesterov_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(1000, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_sgd_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_sgd_nesterov_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_sgd_momentum_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_sgd_momentum_nesterov_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_sgd_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_sgd_nesterov_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_sgd_momentum_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_sgd_momentum_nesterov_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_sgd_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_sgd_nesterov_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_sgd_momentum_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_sgd_momentum_nesterov_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_sgd_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_sgd_nesterov_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_sgd_momentum_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_sgd_momentum_nesterov_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = SGD(lr=0.001, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_adam_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_rmsprop_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_adagrad_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adagrad(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_adadelta_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adadelta(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_adam_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_rmsprop_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_adagrad_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adagrad(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_adadelta_relu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adadelta(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_adam_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_rmsprop_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_adagrad_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adagrad(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_small_adadelta_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=10, strides=10, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='elu'))
        model.add(Dense(200, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adadelta(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_adam_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_rmsprop_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_adagrad_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adagrad(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_large_adadelta_elu(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=10, kernel_size=4, strides=2, activation='elu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=20, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=40, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Conv1D(filters=80, kernel_size=4, strides=2, activation='elu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(1000, activation='elu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adadelta(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ascad_id_winres(self, classes, number_of_samples):
        model = Sequential()
        model.add(AveragePooling1D(pool_size=10, strides=10, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(200, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(200, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.0001)
        # optimizer = AdaBound(lr=1e-3, final_lr=0.1)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_dropout(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.25))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.25))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.25))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.25))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_big(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(1000, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_ascad_id(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        model.add(Dense(600, activation='relu'))
        model.add(Dense(600, activation='relu'))
        model.add(Dense(600, activation='relu'))
        model.add(Dense(600, activation='relu'))
        model.add(Dense(600, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ascad_random_key(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=16, kernel_size=10, strides=5, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(AveragePooling1D(pool_size=2, strides=2))
        model.add(Flatten())
        model.add(Dense(128, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.5))
        model.add(Dense(128, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.00005)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_methodology_paper(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=4, kernel_size=1, strides=1, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(AveragePooling1D(pool_size=2, strides=2))
        model.add(Flatten())
        model.add(Dense(10, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(10, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.00001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_methodology_random(self, classes, number_of_samples):

        self.activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        self.neurons = random.randint(5, 15)
        self.dense_layers = random.randint(1, 3)
        self.filters = random.randint(4, 8)
        self.kernel_size = random.randint(1, 4)
        self.learning_rate = random.uniform(0.000008, 0.000012)

        model = Sequential()
        model.add(
            Conv1D(filters=self.filters, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                   kernel_initializer='he_normal', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(AveragePooling1D(pool_size=2, strides=2))
        model.add(Flatten())
        for l_i in range(self.dense_layers):
            model.add(Dense(self.neurons, activation=self.activation_function, kernel_initializer='he_normal'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=self.learning_rate)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_noise_paper(self, classes, number_of_samples):
        model = Sequential()

        model.add(Conv1D(filters=8, kernel_size=3, strides=1, activation='relu', padding='valid', kernel_initializer='random_uniform'
                         , input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=16, kernel_size=3, strides=1, activation='relu', padding='valid', kernel_initializer='random_uniform'))
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=32, kernel_size=3, strides=1, activation='relu', padding='valid', kernel_initializer='random_uniform'))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=64, kernel_size=3, strides=1, activation='relu', padding='valid', kernel_initializer='random_uniform'))
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=128, kernel_size=3, strides=1, activation='relu', padding='valid', kernel_initializer='random_uniform'))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=256, kernel_size=3, strides=1, activation='relu', padding='valid', kernel_initializer='random_uniform'))
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=256, kernel_size=3, strides=1, activation='relu', padding='valid', kernel_initializer='random_uniform'))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=4, kernel_size=3, strides=1, activation='relu', padding='valid', kernel_initializer='random_uniform'))
        model.add(MaxPooling1D(pool_size=2, strides=1))
        model.add(Dropout(0.5))

        model.add(Flatten())
        model.add(Dense(256, activation='relu', kernel_initializer='random_uniform'))
        model.add(Dense(classes, activation='softmax'))

        model.summary()
        optimizer = Adam(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_noise_paper_random(self, classes, number_of_samples):

        self.activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        self.neurons = random.randint(256, 512)
        self.kernel_size = random.randint(2, 4)
        self.learning_rate = random.uniform(0.00005, 0.00015)

        model = Sequential()

        model.add(Conv1D(filters=8, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                         kernel_initializer='random_uniform'
                         , input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=16, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                         kernel_initializer='random_uniform'))
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=32, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                         kernel_initializer='random_uniform'))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=64, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                         kernel_initializer='random_uniform'))
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=128, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                         kernel_initializer='random_uniform'))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=256, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                         kernel_initializer='random_uniform'))
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=256, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                         kernel_initializer='random_uniform'))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2, strides=1))

        model.add(Conv1D(filters=4, kernel_size=self.kernel_size, strides=1, activation=self.activation_function, padding='valid',
                         kernel_initializer='random_uniform'))
        model.add(MaxPooling1D(pool_size=2, strides=1))
        model.add(Dropout(0.5))

        model.add(Flatten())
        model.add(Dense(self.neurons, activation=self.activation_function, kernel_initializer='random_uniform'))
        model.add(Dense(classes, activation='softmax'))

        model.summary()
        optimizer = Adam(lr=self.learning_rate)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_ascad_small_rmsprop(self, classes, number_of_samples):
        model = Sequential()
        model.add(Dense(200, activation='relu', input_shape=(number_of_samples,)))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_ecc_ha(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        model.add(Dense(800, activation='relu'))
        model.add(Dense(400, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(100, activation='relu'))
        model.add(Dense(50, activation='relu'))
        model.add(Dense(100, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(400, activation='relu'))
        model.add(Dense(800, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_small(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=8, kernel_size=20, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=20, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=20, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_small_dropout(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=8, kernel_size=20, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=20, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=20, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_small_gaussian_noise(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=8, kernel_size=20, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=20, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=20, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(GaussianNoise(1))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(GaussianNoise(1))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=8, kernel_size=40, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_winres_no_opt(self, classes, number_of_samples):
        model = Sequential()
        model.add(AveragePooling1D(pool_size=20, strides=5, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=8, kernel_size=4, strides=1, activation='relu', padding='valid'))
        model.add(MaxPooling1D(pool_size=2, strides=2, padding='valid'))
        model.add(Conv1D(filters=16, kernel_size=4, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=4, strides=1, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

    def cnn_ecc_ha_big(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=64, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0000001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_random(self, classes, number_of_samples):

        self.learning_rate = random.uniform(0.000001, 0.001)
        self.activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        self.optimizer = [RMSprop(lr=self.learning_rate), Adam(lr=self.learning_rate),
                          Adagrad(lr=self.learning_rate), Adadelta(lr=self.learning_rate)][random.randint(0, 3)]
        self.neurons = random.randrange(100, 1000, 10)
        self.dense_layers = random.randint(1, 10)

        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        for layer_index in range(self.dense_layers):
            model.add(Dense(self.neurons, activation=self.activation_function))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=self.optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_random(self, classes, number_of_samples):

        self.learning_rate = random.uniform(0.000001, 0.001)
        self.activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        self.optimizer = [RMSprop(lr=self.learning_rate), Adam(lr=self.learning_rate),
                          Adagrad(lr=self.learning_rate), Adadelta(lr=self.learning_rate)][random.randint(0, 3)]
        # self.optimizer = [SGD(lr=self.learning_rate), SGD(lr=self.learning_rate, momentum=0.9), SGD(lr=self.learning_rate, nesterov=True),
        #                   SGD(lr=self.learning_rate, nesterov=True, momentum=0.9)]
        opt = random.randint(0, 3)
        if opt == 0:
            self.momentum = 0
            self.nesterov = False
        elif opt == 1:
            self.momentum = 0.9
            self.nesterov = False
        elif opt == 2:
            self.momentum = 0
            self.nesterov = True
        else:
            self.momentum = 0.9
            self.nesterov = True

        self.neurons = random.randrange(100, 1010, 10)
        self.conv_layers = random.randint(1, 4)
        self.dense_layers = random.randint(1, 10)
        self.filters = random.randint(4, 8)
        self.kernel_size = random.randint(4, 40)
        self.stride = random.randint(1, 4)

        model = Sequential()
        model.add(
            Conv1D(filters=self.filters, kernel_size=self.kernel_size, strides=self.stride, activation=self.activation_function,
                   padding='valid',
                   input_shape=(number_of_samples, 1)))
        model.add(
            Conv1D(filters=self.filters * 2, kernel_size=self.kernel_size, strides=self.stride, activation=self.activation_function,
                   padding='valid'))
        model.add(
            Conv1D(filters=self.filters * 4, kernel_size=self.kernel_size, strides=self.stride, activation=self.activation_function,
                   padding='valid'))
        model.add(Flatten())
        for layer_index in range(self.dense_layers):
            model.add(Dense(self.neurons, activation=self.activation_function))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer=self.optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    # ----- models for HA DL paper ----#

    def cnn_ecc_ha_pointer(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=8, kernel_size=40, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_pointer_dropout(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=8, kernel_size=40, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_pointer_random(self, classes, number_of_samples):

        activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        neurons = random.randrange(100, 400, 50)
        layers = random.randint(1, 5)
        kernel_size = random.randrange(10, 40, 5)
        stride = random.randrange(1, 4, 1)
        filters = random.randrange(4, 8, 1)

        model = Sequential()
        model.add(Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=filters * 2, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 4, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dense(neurons, activation=activation_function, kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_pointer_dropout_random(self, classes, number_of_samples):
        activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        neurons = random.randrange(100, 400, 50)
        layers = random.randint(1, 5)
        kernel_size = random.randrange(10, 40, 5)
        stride = random.randrange(1, 4, 1)
        filters = random.randrange(4, 8, 1)

        model = Sequential()
        model.add(Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=filters * 2, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 4, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dropout(0.5))
            model.add(Dense(neurons, activation=activation_function, kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_no_opt(self, classes, number_of_samples):
        model = Sequential()
        model.add(AveragePooling1D(pool_size=4, strides=4, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=8, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(MaxPooling1D(pool_size=2, strides=2, padding='valid'))
        model.add(Conv1D(filters=16, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_no_opt_dropout(self, classes, number_of_samples):
        model = Sequential()
        model.add(AveragePooling1D(pool_size=4, strides=4, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=8, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(MaxPooling1D(pool_size=2, strides=2, padding='valid'))
        model.add(Conv1D(filters=16, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_no_opt_random(self, classes, number_of_samples):

        activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        neurons = random.randrange(100, 400, 50)
        layers = random.randint(1, 5)
        kernel_size = random.randrange(10, 40, 5)
        stride = random.randrange(1, 4, 1)
        filters = random.randrange(4, 8, 1)

        model = Sequential()
        model.add(AveragePooling1D(pool_size=4, strides=4, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 2, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 4, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dense(neurons, activation=activation_function, kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_no_opt_dropout_random(self, classes, number_of_samples):
        activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        neurons = random.randrange(100, 400, 50)
        layers = random.randint(1, 5)
        kernel_size = random.randrange(10, 40, 5)
        stride = random.randrange(1, 4, 1)
        filters = random.randrange(4, 8, 1)

        model = Sequential()
        model.add(AveragePooling1D(pool_size=4, strides=4, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 2, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 4, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dropout(0.5))
            model.add(Dense(neurons, activation=activation_function, kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    # -- optimized GV for HA DL paper --- #

    def cnn_ecc_ha_pointer_gv(self, classes, number_of_samples):
        model = Sequential()
        model.add(AveragePooling1D(pool_size=1, strides=1, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=8, kernel_size=10, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=16, kernel_size=10, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=10, strides=1, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_pointer_dropout_gv(self, classes, number_of_samples):
        model = Sequential()
        model.add(AveragePooling1D(pool_size=1, strides=1, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=8, kernel_size=10, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=16, kernel_size=10, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=10, strides=1, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_pointer_random_gv(self, classes, number_of_samples):

        activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        neurons = random.randrange(100, 400, 50)
        layers = random.randint(1, 5)
        kernel_size = random.randrange(5, 10, 5)
        stride = random.randrange(1, 2, 1)
        filters = random.randrange(4, 8, 1)

        model = Sequential()
        model.add(AveragePooling1D(pool_size=1, strides=1, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 2, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 4, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dense(neurons, activation=activation_function, kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_pointer_dropout_random_gv(self, classes, number_of_samples):
        activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        neurons = random.randrange(100, 400, 50)
        layers = random.randint(1, 5)
        kernel_size = random.randrange(5, 10, 5)
        stride = random.randrange(1, 2, 1)
        filters = random.randrange(4, 8, 1)

        model = Sequential()
        model.add(AveragePooling1D(pool_size=1, strides=1, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 2, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 4, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dropout(0.5))
            model.add(Dense(neurons, activation=activation_function, kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_no_opt_gv(self, classes, number_of_samples):
        model = Sequential()
        model.add(AveragePooling1D(pool_size=1, strides=1, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=8, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(MaxPooling1D(pool_size=2, strides=2, padding='valid'))
        model.add(Conv1D(filters=16, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_no_opt_dropout_gv(self, classes, number_of_samples):
        model = Sequential()
        model.add(AveragePooling1D(pool_size=1, strides=1, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=8, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(MaxPooling1D(pool_size=2, strides=2, padding='valid'))
        model.add(Conv1D(filters=16, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=20, strides=1, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu', kernel_initializer='he_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_no_opt_random_gv(self, classes, number_of_samples):

        activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        neurons = random.randrange(100, 400, 50)
        layers = random.randint(1, 5)
        kernel_size = random.randrange(10, 20, 5)
        stride = random.randrange(1, 4, 1)
        filters = random.randrange(4, 8, 1)

        model = Sequential()
        model.add(AveragePooling1D(pool_size=1, strides=1, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 2, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 4, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dense(neurons, activation=activation_function, kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_no_opt_dropout_random_gv(self, classes, number_of_samples):
        activation_function = ['relu', 'tanh', 'selu', 'elu'][random.randint(0, 3)]
        neurons = random.randrange(100, 400, 50)
        layers = random.randint(1, 5)
        kernel_size = random.randrange(10, 20, 5)
        stride = random.randrange(1, 4, 1)
        filters = random.randrange(4, 8, 1)

        model = Sequential()
        model.add(AveragePooling1D(pool_size=1, strides=1, padding='valid', input_shape=(number_of_samples, 1)))
        model.add(BatchNormalization())
        model.add(Conv1D(filters=filters, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 2, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Conv1D(filters=filters * 4, kernel_size=kernel_size, strides=stride, activation=activation_function, padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dropout(0.5))
            model.add(Dense(neurons, activation=activation_function, kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ascad_random_key_2D(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(40, 35, 1)))
        model.add(MaxPooling2D((2, 2)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D((2, 2)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model
